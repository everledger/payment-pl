import React, {useState, useRef, useEffect} from "react";
import { Link } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Box from '@material-ui/core/Box';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';

import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';

import useDataApi from "../use_data_api/usedataapi"

const useStyles = makeStyles(theme => ({
    root: {
      width: '85%',
    },
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'none',
    },
    formControl: {
      display: 'flex',
      textAlign: 'left',
      position: 'relative',
      left: '30px',
    },
  }));
  
    
  function HorizontalLinearStepper() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const steps = getSteps();

    const [{ data, isLoading, isError }, doFetch] = useDataApi();    

    // contact details
    const [contact_name, setContactname] = useState("");
    const [contact_email, setContactemail] = useState("");
    const [contact_psw, setContactpsw] = useState("");
    const [contact_phone, setContactphone] = useState("");

    //company details
    const [company_name, setCompanyname] = useState("");
    const [company_trade_name, setCompanytradename] = useState("");
    const [company_reg_num, setCompanyregnum] = useState("");
    const [inc_locality, setInclocality] = useState("");
    const [mykycbank_id, setMykycbankid] = useState("");
    const [c_adrs_city, setCadrscity] = useState("");
    const [c_adrs_country, setCadrscountry] = useState("");
    const [c_adrs_street, setCadrsstreet] = useState("");
    const [c_adrs_number, setCadrsnumber] = useState("");
    const [c_adrs_postalcode, setCadrspostalcode] = useState("");
    const [c_adrs_region, setCadrsregion] = useState("");
    const [c_adrs_refine, setCadrsrefine] = useState("");


    // Ultimate beneficial owners
    const [ubo_is_person, setUBOisperson] = useState(true);
    const ubo_p_obj = useRef({});
    const ubo_c_obj = useRef({});
    const ubo_p_nationality_arr = useRef([]);
    const ubo_arr = useRef([]);

    // UBO Person
    const ubo_p_name = useRef(null);
    const ubo_p_dob = useRef(null);
    const ubo_p_percent = useRef(null);
    const ubo_p_adrs_city = useRef(null);
    const ubo_p_adrs_country = useRef(null);
    const ubo_p_adrs_street = useRef(null);
    const ubo_p_adrs_number = useRef(null);
    const ubo_p_adrs_postalcode = useRef(null);
    const ubo_p_adrs_region = useRef(null);
    const ubo_p_adrs_refine = useRef(null);

    const [ubo_p_nationality, setUBOpnationality] = useState("");
    const [ubo_p_is_pep, setUBOpispep] = useState(false);
    const [ubo_p_pep_notes, setUBOpepnotes] = useState("");

    function initUBOpersonRefs(){

        ubo_p_name.current.value = "";
        ubo_p_dob.current.value = "";
        ubo_p_percent.current.value = "";
        ubo_p_adrs_city.current.value = "";
        ubo_p_adrs_country.current.value = "";
        ubo_p_adrs_street.current.value = "";
        ubo_p_adrs_number.current.value = "";
        ubo_p_adrs_postalcode.current.value = "";
        ubo_p_adrs_region.current.value = "";
        ubo_p_adrs_refine.current.value = "";

        ubo_p_nationality_arr.current = [];
        setUBOpnationality("")
        setUBOpispep(false)
        setUBOpepnotes("")
    }

    const updateUBOpersonpeptype = e => {
      ubo_p_obj.current = {
        ...ubo_p_obj.current,
        ubo_p_pep_type : e.target.value
      };
    };

    const updateUBOpersonpepnotes = e => {
      setUBOpepnotes(e.target.value)
      ubo_p_obj.current = {
        ...ubo_p_obj.current,
        [e.target.id]: e.target.value
      };
    };


    const updateUBOperson = e => {
        //console.log(e.target.id + ":" + e.target.value)
        ubo_p_obj.current = {
          ...ubo_p_obj.current,
          [e.target.id]: e.target.value
        };
    };

    function addNationality(){
      ubo_p_nationality_arr.current = [...ubo_p_nationality_arr.current, ubo_p_nationality];
      ubo_p_obj.current = {
        ...ubo_p_obj.current,
        nationality: ubo_p_nationality_arr.current
      };
      setUBOpnationality("")
    }


    // UBO Company
    const ubo_c_name = useRef(null);
    const ubo_c_percent = useRef(null);
    const ubo_c_adrs_city = useRef(null);
    const ubo_c_adrs_country = useRef(null);
    const ubo_c_adrs_street = useRef(null);
    const ubo_c_adrs_number = useRef(null);
    const ubo_c_adrs_postalcode = useRef(null);
    const ubo_c_adrs_region = useRef(null);
    const ubo_c_adrs_refine = useRef(null);

    function initUBOcompanyRefs(){

        ubo_c_name.current.value = "";
        ubo_c_percent.current.value = "";
        ubo_c_adrs_city.current.value = "";
        ubo_c_adrs_country.current.value = "";
        ubo_c_adrs_street.current.value = "";
        ubo_c_adrs_number.current.value = "";
        ubo_c_adrs_postalcode.current.value = "";
        ubo_c_adrs_region.current.value = "";
        ubo_c_adrs_refine.current.value = "";

    }

    const updateUBOcompany = e => {
      ubo_c_obj.current = {
          ...ubo_c_obj.current,
          [e.target.id]: e.target.value
      };
    };

    function addUBO(){

      // person
      if (Object.values(ubo_p_obj.current).length > 0){
        addNationality()
        ubo_p_obj.current = {
          ...ubo_p_obj.current,
          is_person: true
        };
        ubo_arr.current = [...ubo_arr.current, ubo_p_obj.current];
        initUBOpersonRefs();
        ubo_p_obj.current = {}
      }

      // company
      if (Object.values(ubo_c_obj.current).length > 0){
        ubo_c_obj.current = {
          ...ubo_c_obj.current,
          is_person: false
        };
        ubo_arr.current = [...ubo_arr.current, ubo_c_obj.current];
        initUBOcompanyRefs();
        ubo_c_obj.current = {};
      }

      //console.log(ubo_arr.current)

    }

    function addUBO_person(){
      addUBO()
      setUBOisperson(true)
    }

    function addUBO_company(){
      addUBO()
      setUBOisperson(false)
    }

    // Directors
    const [director_is_person, setDirectorisperson] = useState(true);
    const director_p_obj = useRef({});
    const director_c_obj = useRef({});
    const directors_arr = useRef([]);

    // Director Person
    const director_p_name = useRef(null);
    const director_p_dob = useRef(null);

    const [director_p_is_pep, setDirectorpispep] = useState(false);
    const [director_p_pep_notes, setDirectorpepnotes] = useState("");

    function initDirectorpersonRefs(){

        director_p_name.current.value = "";
        director_p_dob.current.value = "";

        setDirectorpispep(false);
        setDirectorpepnotes("");
    }

    const updateDirectorpersonpeptype = e => {
      director_p_obj.current = {
        ...director_p_obj.current,
        director_p_pep_type : e.target.value
      };
    };

    const updateDirectorpersonpepnotes = e => {
      setDirectorpepnotes(e.target.value)
      director_p_obj.current = {
        ...director_p_obj.current,
        [e.target.id]: e.target.value
      };
    };


    const updateDirectorperson = e => {
        //console.log(e.target.id + ":" + e.target.value)
        director_p_obj.current = {
          ...director_p_obj.current,
          [e.target.id]: e.target.value
        };
    };

    // Director Company
    const director_c_name = useRef(null);

    function initDirectorcompanyRefs(){

        director_c_name.current.value = "";

    }

    const updateDirectorcompany = e => {
      director_c_obj.current = {
          ...director_c_obj.current,
          [e.target.id]: e.target.value
      };
    };

    function addDirector(){

      // person
      if (Object.values(director_p_obj.current).length > 0){
        director_p_obj.current = {
          ...director_p_obj.current,
          is_person: true
        };
        directors_arr.current = [...directors_arr.current, director_p_obj.current];
        initDirectorpersonRefs();
        director_p_obj.current = {}
      }

      // company
      if (Object.values(director_c_obj.current).length > 0){
        director_c_obj.current = {
          ...director_c_obj.current,
          is_person: false
        };
        directors_arr.current = [...directors_arr.current, director_c_obj.current];
        initDirectorcompanyRefs();
        director_c_obj.current = {};
      }

      //console.log(directors_arr.current)

    }

    function addDirector_person(){
      addDirector()
      setDirectorisperson(true)
    }

    function addDirector_company(){
      addDirector()
      setDirectorisperson(false)
    }

    // exchanges
    const [listedCheck, setListedcheck] = useState(false);
    const exchanges_arr = useRef([]);
    const exchange_obj = useRef({});

    const stock_exchange_name = useRef(null);
    const stock_exchange_ticker = useRef(null);

    function initExchangeRefs(){

      stock_exchange_name.current.value = "";
      stock_exchange_ticker.current.value = "";

    }

    const updateExchange = e => {
      exchange_obj.current = {
        ...exchange_obj.current,
        [e.target.id]: e.target.value
      };
    };

    function addExchange(){
      if (Object.values(exchange_obj.current).length > 0){
        exchanges_arr.current = [...exchanges_arr.current, exchange_obj.current];
        initExchangeRefs();
        exchange_obj.current = {};
      }
    }

    // trading address
    const tradingAdrs_arr = useRef([]);
    const tradingAdrs_obj = useRef({});

    const c_t_adrs_city = useRef(null);
    const c_t_adrs_country = useRef(null);
    const c_t_adrs_street = useRef(null);
    const c_t_adrs_number = useRef(null);
    const c_t_adrs_postalcode = useRef(null);
    const c_t_adrs_region = useRef(null);
    const c_t_adrs_refine = useRef(null);

    function initTradingAddressesRefs(){

      c_t_adrs_city.current.value = "";
      c_t_adrs_country.current.value = "";
      c_t_adrs_street.current.value = "";
      c_t_adrs_number.current.value = "";
      c_t_adrs_postalcode.current.value = "";
      c_t_adrs_region.current.value = "";
      c_t_adrs_refine.current.value = "";

    }

    const updateTradingAdrs = e => {
      tradingAdrs_obj.current = {
        ...tradingAdrs_obj.current,
        [e.target.id]: e.target.value
      };
    };

    function addTradingAdrs(){
      if (Object.values(tradingAdrs_obj.current).length > 0){
        tradingAdrs_arr.current = [...tradingAdrs_arr.current, tradingAdrs_obj.current];
        initTradingAddressesRefs();
        tradingAdrs_obj.current = {};
      }
    }

    // beneficiary
    const [sort_code, setSortcode] = useState("");
    const [account_num, setAccountno] = useState("");


    function getSteps() {
        return ['Contact details', 'Company details', 'Directors', 'Ultimate Beneficial Owners' , 'Company Beneficiary'];
    }
  
    function handleNext() {

      if (activeStep === 1){
        addTradingAdrs()
        addExchange()
      }
      if (activeStep === 2){
        addDirector()
      }
      if (activeStep === 3){
        addUBO()
      }
      
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      if (activeStep === steps.length - 1){
        doFetch(
            encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler_pl.jsp?callType=register&contact_name=' + contact_name + '&contact_email=' + contact_email + '&contact_psw=' + contact_psw + '&contact_phone=' + contact_phone + '&company_name=' + company_name + '&company_trade_name=' + company_trade_name + '&company_reg_num=' + company_reg_num + '&inc_locality=' + inc_locality + '&mykycbank_id=' + mykycbank_id + '&c_adrs_city=' + c_adrs_city + '&c_adrs_country=' + c_adrs_country + '&c_adrs_street=' + c_adrs_street + '&c_adrs_number=' + c_adrs_number + '&c_adrs_postalcode=' + c_adrs_postalcode + '&c_adrs_region=' + c_adrs_region + '&c_adrs_refine=' + c_adrs_refine + '&exchanges_arr=' + JSON.stringify(exchanges_arr.current) + '&tradingadrs_arr=' + JSON.stringify(tradingAdrs_arr.current) + '&directors_arr=' + JSON.stringify(directors_arr.current) + '&ubo_arr=' + JSON.stringify(ubo_arr.current) + '&sort_code=' + sort_code + '&account_num=' + account_num),
        );
      }
    }
  
    function handleBack() {
      setActiveStep(prevActiveStep => prevActiveStep - 1);
    }

    //useEffect(() => console.log(ubo_arr.current), [ubo_arr.current]);
    

    function getStepContent() {
  
        switch (activeStep) {
          case 0:
            return <Box>
                    <br/>
                      <TextField
                        id="contact_name"
                        label="Contact Name"
                        onChange={e => setContactname(e.target.value)}
                        value={contact_name}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>account_circle</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />            
                      <br/><br/>
                      <TextField
                        id="contact_email"
                        label="Contact Email"
                        onChange={e => setContactemail(e.target.value)}
                        value={contact_email}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>email</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <br/><br/>
                      <TextField
                        id="contact_password"
                        label="Password"
                        type="password"
                        onChange={e => setContactpsw(e.target.value)}
                        value={contact_psw}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>lock</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <br/><br/>
                      <TextField
                        id="contact_phone"
                        label="Contact Phone"
                        onChange={e => setContactphone(e.target.value)}
                        value={contact_phone}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>phone</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <br/><br/>
                      <Checkbox color="default" /> I agree to terms of use
                      <br/><br/><br/>
                    </Box>;
          case 1:
            return <Box>
                    <span className="row">
                        <span className="col">
                            <TextField
                                id="company_name"
                                label="Company Name"
                                onChange={e => setCompanyname(e.target.value)}
                                value={company_name}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>store</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_trade_name"
                                label="Company Trading Name"
                                onChange={e => setCompanytradename(e.target.value)}
                                value={company_trade_name}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>store</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_reg_num"
                                label="Registration Number"
                                onChange={e => setCompanyregnum(e.target.value)}
                                value={company_reg_num}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>account_box</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="inc_locality"
                                label="Incorporation Locality"
                                onChange={e => setInclocality(e.target.value)}
                                value={inc_locality}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="mykycbank_id"
                                label="MyKYCBank Profile ID"
                                onChange={e => setMykycbankid(e.target.value)}
                                value={mykycbank_id}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>account_box</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/><br/>
                            <FormControlLabel
                              control={
                                <Checkbox
                                checked={listedCheck}
                                onChange={e => setListedcheck(!listedCheck)}
                                value={listedCheck}
                                color="default"
                                />
                              }
                              label="Listed on stock exchange"
                            />
                            <br/>
                            {listedCheck? (
                              <span>
                                <TextField
                                    id="stock_exchange_name"
                                    inputRef={stock_exchange_name}
                                    label="Exchange Name"
                                    onChange={updateExchange}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Icon>account_balance</Icon>
                                        </InputAdornment>
                                    ),
                                    }}
                                />
                                <br/><br/>
                                <TextField
                                    id="stock_exchange_ticker"
                                    inputRef={stock_exchange_ticker}
                                    label="Exchange Ticker"
                                    onChange={updateExchange}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Icon>account_balance</Icon>
                                        </InputAdornment>
                                    ),
                                    }}
                                />
                                <br/><br/>
                                <Button onClick={addExchange} variant="outlined" className={classes.button}>
                                    + Add another exchange
                                </Button>
                              </span>
                            ) : ("")}
                        </span>
                        <span className="col">
                          <span className="col_title">Registration Address:</span>
                            <TextField
                                id="company_street"
                                label="Street"
                                onChange={e => setCadrsstreet(e.target.value)}
                                value={c_adrs_street}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_street_number"
                                label="Number"
                                onChange={e => setCadrsnumber(e.target.value)}
                                value={c_adrs_number}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_city"
                                label="City"
                                onChange={e => setCadrscity(e.target.value)}
                                value={c_adrs_city}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/><br/>
                            <TextField
                                id="company_region"
                                label="Region"
                                onChange={e => setCadrsregion(e.target.value)}
                                value={c_adrs_region}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_postal_code"
                                label="Postal code"
                                onChange={e => setCadrspostalcode(e.target.value)}
                                value={c_adrs_postalcode}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_country"
                                label="Country ISO"
                                onChange={e => setCadrscountry(e.target.value)}
                                value={c_adrs_country}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_refine"
                                label="Address Refinement"
                                onChange={e => setCadrsrefine(e.target.value)}
                                value={c_adrs_refine}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                        </span>
                        <span className="col">
                          <span className="col_title">Trading Address:</span>
                          <TextField
                                id="company_street_t"
                                label="Street"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_street}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_street_number_t"
                                label="Number"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_number}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_city_t"
                                label="City"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_city}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/><br/>
                            <TextField
                                id="company_region_t"
                                label="Region"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_region}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_postal_code_t"
                                label="Postal code"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_postalcode}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_country_t"
                                label="Country ISO"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_country}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="company_refine_t"
                                label="Address Refinement"
                                onChange={updateTradingAdrs}
                                inputRef={c_t_adrs_refine}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <Button onClick={addTradingAdrs} variant="outlined" className={classes.button}>
                                + Add another trading address
                            </Button>

                        </span>
                    </span>
                    </Box>;
          case 2:
            return <Box>

                      Directors:
                      <Button onClick={addDirector_person} variant="outlined" className={classes.button}>
                          + Add Person
                      </Button>
                      <Button onClick={addDirector_company} variant="outlined" className={classes.button}>
                          + Add Company
                      </Button>
                      <br/>
                      {director_is_person ? (
                        <span className="row">
                          <span className="col">
                            <span className="col_title">Person:</span>
                            <TextField
                            id="director_p_name"
                            inputRef={director_p_name}
                            label="Name "
                            onChange={updateDirectorperson}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Icon>account_circle</Icon>
                                </InputAdornment>
                            ),
                            }}
                            />
                            <br/>
                            <TextField
                                id="director_p_dob"
                                inputRef={director_p_dob}
                                label="Date of Birth"
                                onChange={updateDirectorperson}
                                type="date"
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>event</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                          </span>
                          <span className="col">
                            <FormControlLabel
                              control={
                                <Checkbox
                                checked={director_p_is_pep}
                                onChange={e => setDirectorpispep(!director_p_is_pep)}
                                value={director_p_is_pep}
                                color="default"
                                />
                              }
                              label="Politically Exposed Person"
                            />
                            <br/>
                            {director_p_is_pep ? (
                              <span>

                                <FormControl component="fieldset" className={classes.formControl}>
                                  <FormLabel component="legend">PEP Type</FormLabel>
                                  <RadioGroup
                                    name="director_p_pep_type"
                                    onChange={updateDirectorpersonpeptype}
                                  >
                                    <FormControlLabel value="direct" control={<Radio color="default" />} label="Direct" />
                                    <FormControlLabel value="close-associate" control={<Radio color="default" />} label="Close associate" />
                                    <FormControlLabel value="former-pep" control={<Radio color="default" />} label="Former PEP" />
                                    <FormControlLabel value="family-member" control={<Radio color="default" />} label="Family member" />
                                  </RadioGroup>
                                </FormControl>

                                <br/><br/>
                                <TextField
                                    id="director_p_pep_notes"
                                    value={director_p_pep_notes}
                                    label="PEP Notes (e.g politician)"
                                    onChange={updateDirectorpersonpepnotes}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Icon>comment</Icon>
                                        </InputAdornment>
                                    ),
                                    }}
                                />

                              </span>
                            ) : (
                              ""
                            )}
                          </span> 
                          <span className="col">
                          </span>
                        </span> 
                      ) : (
                        <span className="row">
                          <span className="col">
                            <span className="col_title">Company:</span>
                            <TextField
                            id="director_c_name"
                            inputRef={director_c_name}
                            label="Name"
                            onChange={updateDirectorcompany}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Icon>account_circle</Icon>
                                </InputAdornment>
                            ),
                            }}
                            />
                          </span>
                          <span className="col">
                          </span>
                          <span className="col">
                          </span> 
                        </span>
                      )}

                </Box>;

          case 3:
            return <Box>
                      Ultimate Beneficial Owners:
                      <Button onClick={addUBO_person} variant="outlined" className={classes.button}>
                          + Add Person
                      </Button>
                      <Button onClick={addUBO_company} variant="outlined" className={classes.button}>
                          + Add Company
                      </Button>
                      <br/>
                      {ubo_is_person ? (
                        <span className="row">
                          <span className="col">
                            <span className="col_title">Person:</span>
                            <TextField
                            id="ubo_p_name"
                            inputRef={ubo_p_name}
                            label="Name"
                            onChange={updateUBOperson}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Icon>account_circle</Icon>
                                </InputAdornment>
                            ),
                            }}
                            />
                            <br/>
                            <TextField
                                id="ubo_p_dob"
                                inputRef={ubo_p_dob}
                                label="Date Of Birth"
                                onChange={updateUBOperson}
                                type="date"
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>event</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/>
                            <TextField
                            id="ubo_p_percent"
                            inputRef={ubo_p_percent}
                            label="Percentage Holding"
                            onChange={updateUBOperson}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    %
                                </InputAdornment>
                            ),
                            }}
                            />
                            <br/>
                            <TextField
                              id="ubo_p_nationality"
                              label="Nationality"
                              onChange={e => setUBOpnationality(e.target.value)}
                              value={ubo_p_nationality}
                              InputProps={{
                                startAdornment: (
                                  <InputAdornment position="start">
                                      <Icon>language</Icon>
                                  </InputAdornment>
                                ),
                              }}
                            />
                            <br/>
                            <Button onClick={addNationality} variant="outlined" className={classes.button}>
                                + Add Nationality
                            </Button>
                          </span>
                          <span className="col">
                            <TextField
                                id="ubo_p_adrs_street"
                                label="Street"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_street}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_number"
                                label="Number"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_number}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_city"
                                label="City"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_city}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_region"
                                label="Region"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_region}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_postalcode"
                                label="Postal code"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_postalcode}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_country"
                                label="Country ISO"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_country}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_p_adrs_refine"
                                label="Address Refinement"
                                onChange={updateUBOperson}
                                inputRef={ubo_p_adrs_refine}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                          </span>
                          <span className="col">
                            <FormControlLabel
                              control={
                                <Checkbox
                                checked={ubo_p_is_pep}
                                onChange={e => setUBOpispep(!ubo_p_is_pep)}
                                value={ubo_p_is_pep}
                                color="default"
                                />
                              }
                              label="Politically Exposed Person"
                            />
                            <br/>
                            {ubo_p_is_pep ? (
                              <span>

                                <FormControl component="fieldset" className={classes.formControl}>
                                  <FormLabel component="legend">PEP Type</FormLabel>
                                  <RadioGroup
                                    name="ubo_p_pep_type"
                                    onChange={updateUBOpersonpeptype}
                                  >
                                    <FormControlLabel value="direct" control={<Radio color="default" />} label="Direct" />
                                    <FormControlLabel value="close-associate" control={<Radio color="default" />} label="Close associate" />
                                    <FormControlLabel value="former-pep" control={<Radio color="default" />} label="Former PEP" />
                                    <FormControlLabel value="family-member" control={<Radio color="default" />} label="Family member" />
                                  </RadioGroup>
                                </FormControl>

                                <br/><br/>
                                <TextField
                                    id="ubo_p_pep_notes"
                                    value={ubo_p_pep_notes}
                                    label="PEP Notes (e.g politician)"
                                    onChange={updateUBOpersonpepnotes}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Icon>comment</Icon>
                                        </InputAdornment>
                                    ),
                                    }}
                                />

                              </span>
                            ) : (
                              ""
                            )}
                          </span> 
                        </span> 
                      ) : (
                        <span className="row">
                          <span className="col">
                            <span className="col_title">Company:</span>
                            <TextField
                            id="ubo_c_name"
                            inputRef={ubo_c_name}
                            label="Name"
                            onChange={updateUBOcompany}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Icon>account_circle</Icon>
                                </InputAdornment>
                            ),
                            }}
                            />
                            <br/>
                            <TextField
                            id="ubo_c_percent"
                            inputRef={ubo_c_percent}
                            label="Percentage Holding"
                            onChange={updateUBOcompany}
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    %
                                </InputAdornment>
                            ),
                            }}
                            />
                          </span>
                          <span className="col">
                            <span className="col_title">Registration Address:</span>
                            <TextField
                                id="ubo_c_adrs_street"
                                label="Street"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_street}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_number"
                                label="Number"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_number}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_city"
                                label="City"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_city}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_region"
                                label="Region"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_region}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_postalcode"
                                label="Postal code"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_postalcode}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_country"
                                label="Country ISO"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_country}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                            <br/><br/>
                            <TextField
                                id="ubo_c_adrs_refine"
                                label="Address Refinement"
                                onChange={updateUBOcompany}
                                inputRef={ubo_c_adrs_refine}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Icon>location_city</Icon>
                                    </InputAdornment>
                                ),
                                }}
                            />            
                          </span>
                          <span className="col">
                          </span> 
                        </span>
                      )}
                  </Box>;

          case 4:
            return <Box>
                    <br/>
                      <TextField
                        id="sort_code"
                        label="UK Sort Code"
                        onChange={e => setSortcode(e.target.value)}
                        value={sort_code}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>account_balance</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />            
                      <br/><br/>
                      <TextField
                        id="account_no"
                        label="UK Account Number"
                        onChange={e => setAccountno(e.target.value)}
                        value={account_num}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                                <Icon>account_balance</Icon>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <br/><br/><br/>
                    </Box>;

      
          default:
            return 'Unknown step';
        }
      }
    
  
    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
                <br/><br/>
                {isLoading ? (
                <div>Loading ...</div>
                ) : (
                <div>
                    {isError ? (
                        <div>Something went wrong ...</div>
                    ) : (
                        <Typography className={classes.instructions}>
                        Thanks for joining!
                        <br/>
                        KYC process has started...
                        <br/>
                        We'll contact you soon.
                      </Typography>        
                    )}
                </div>
                )}
            </div>
          ) : (
            <div>
              <Typography component={'span'} className={classes.instructions}>{getStepContent()}</Typography>
              <div>
              {activeStep === 0 ? (
                <Button component={Link} to="/" className={classes.button}>
                  Back
                </Button>
              ) : (
                <Button onClick={handleBack} className={classes.button}>
                  Back
                </Button>
              )}
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.button}
                >
                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
  
  
  function Register() {
  
    return  <center>
              <br/><br/>
              <HorizontalLinearStepper/>
            </center>;
  }

  export default Register;
  