import React, {useState, useEffect} from "react";
import { Link, Redirect } from "react-router-dom";
import { useGlobal} from 'reactn';

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import useDataApi from "../use_data_api/usedataapi"

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function Login() {

    const classes = useStyles();

    const [email, setEmail] = useState("");
    const [psw, setpsw] = useState("");
    const [ global, setGlobal ] = useGlobal();
    const [isloggedin, setIsloggedin] = useState(false);

    //setGlobal({ user_id: 0 });

    const [{ data, isLoading, isError }, doFetch] = useDataApi(null,{user_id:0});

    function do_login(){

      doFetch(
        encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler_pl.jsp?callType=login&email=' + email + '&psw=' + psw),
      );

    }

    function set_user_id(){
      setGlobal({user_id:data.user_id})
      setIsloggedin(true)
    }

    useEffect(() => {
      if (data.user_id > 0){
        set_user_id()
      }
    }
    , [data.user_id]);

    if (isloggedin){

      return <Redirect to='/myaccount' />

    }else{

      return  <center>
      <br/><br/><br/><br/>
          <TextField
            id="email"
            label="Email"
            onChange={e => setEmail(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon>email</Icon>
                </InputAdornment>
              ),
            }}
          />
          <br/><br/>
          <TextField
            id="password"
            label="Password"
            onChange={e => setpsw(e.target.value)}
            type="password"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon>lock</Icon>
                </InputAdornment>
              ),
            }}
          />
          <br/><br/><br/>
          <Button component={Link} to="/" className={classes.button}>
          Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={do_login}
          >
            Login
          </Button>
      </center>;

    }

}

  export default Login;
  