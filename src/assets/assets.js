import React, {useState, useEffect} from "react";
import { Link, Redirect } from "react-router-dom";
import { useGlobal} from 'reactn';

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import Modal from '@material-ui/core/Modal';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';


//import TextField from '@material-ui/core/TextField';
//import InputAdornment from '@material-ui/core/InputAdornment';

import useDataApi from "../use_data_api/usedataapi"

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  card: {
    margin: theme.spacing(1),
  },
}));


function Assets() {

    const classes = useStyles();

    const [global, setGlobal] = useGlobal();
    const [modalOpen, setModalOpen] = useState(false);
    const [modalURL, setModalURL] = useState("");

    const [{ data, isLoading, isError }, doFetch] = useDataApi(
      'http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler_pl.jsp?callType=get_assets&user_id=' + global.user_id,
      {low_balance:false, assets:[{asset_id:0,cert_id:0,img:"",carat:"",clarity:"",color:"",cut:"",price:"",url:"",company:""}]}
    );  

    const handleModalOpen = (url) => {
      setModalURL(url)
      setModalOpen(true);
    };
  
    const handleModalClose = () => {
      setModalOpen(false);
    };

    function buy(asset_id){

      doFetch(
        encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler_pl.jsp?callType=buy&user_id=' + global.user_id + '&asset_id=' + asset_id),
      );
      setOpenlowbalance(true);

    }

    const [openLowBalance, setOpenlowbalance] = useState(false);

    function lowbalanceOpen() {
      setOpenlowbalance(true);
    }
  
    function lowbalanceClose(event) {
      setOpenlowbalance(false);
    }
  

    if (global.user_id>0){

      return  <Box>
          <br/><br/><span className="pagetitle">Diamonds for sale</span>

          <Snackbar
            anchorOrigin={{
              vertical: 'center',
              horizontal: 'center',
            }}
            open={openLowBalance && data.low_balance && !isLoading}
            onClose={lowbalanceClose}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">Insufficient funds for this transaction request</span>}
            action={[
              <IconButton
                key="close"
                aria-label="close"
                color="inherit"
                className={classes.close}
                onClick={lowbalanceClose}
              >
                <Icon>close</Icon>
              </IconButton>,
            ]}
          />

          <span style={{position:"absolute",right:"10%"}}>
          <Button component={Link} to="/myaccount" size="large" variant="outlined" className={classes.button}>
                My Account&nbsp;&nbsp;
                <i className="material-icons">
                account_balance_wallet
                </i>
          </Button>
          </span>
          <br/><br/>
          <Modal open={modalOpen} onClose={handleModalClose}>
            <div style={{width:"70%",height:"80%",left:"15%",top:"10%",position:"absolute"}}>
              <iframe src={modalURL} width="100%" height="100%"></iframe>
            </div>
          </Modal>

          <br/><br/>
          {isLoading ? (
            <span><CircularProgress /></span>
          ) : (
            data.assets.map((asset,index)=>{
            return <Card key={asset.asset_id} className={classes.card}>
                      <CardContent>
                        <img src={asset.img} width="150px" style={{float:"left",margin:"15px"}}></img>
                        <br/>
                        <span className="grey_it">{asset.cert_id}</span>
                        <br/><br/>
                        <span>
                          <span className="bold_it">{asset.carat}</span> <span className="grey_it">Carat</span> | <span className="bold_it">{asset.clarity}</span> <span className="grey_it">Clarity</span> | <span className="bold_it">{asset.color}</span> <span className="grey_it">color</span> | <span className="bold_it">{asset.cut}</span> <span className="grey_it">Cut</span>
                        </span>
                        <br/><br/>
                        <span className="bold_it">
                          £{asset.price}
                        </span>
                        <br/><br/>
                        <span className="grey_it">
                          Owner: {asset.company}
                        </span>
                        
                      </CardContent>
                      <CardActions>
                        <Button variant="outlined" color="primary" onClick={()=>handleModalOpen(asset.url)}>Provenance details</Button>
                        <Button variant="contained" color="primary" onClick={()=>buy(asset.asset_id)}>Buy</Button>
                      </CardActions>
                    </Card>
            })
          )}

        </Box>;

    }else{

      return <Redirect to='/login' />

    }

}

  export default Assets;
  