import React from "react";
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';


import Diamond_png from './images/polished-diamond.png';
import el_logo from './images/EL_logo_email.png';
import railsbank from './images/railsbank.jpg';

import Register from "./register/register"
import Login from "./login/login"
import Myaccount from "./myaccount/myaccount"
import Assets from "./assets/assets"
import Upload from "./upload/upload"

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));

function App() {
  return (
    <Router basename="/payment">
      <div>
        <Header />
        <Container>

          <Route exact path="/" component={Login_btns} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/myaccount" component={Myaccount} />
          <Route path="/assets" component={Assets} />
          <Route path="/upload" component={Upload} />

        </Container>

        <br/><br/><br/><br/>
        <Footer />

      </div>
    </Router>
  );
}


function Login_btns(){
  const classes = useStyles();

  return <center>
          <br/><br/><br/><br/><br/>
          <Button component={Link} to="/login" size="large" variant="outlined" className={classes.button}>
          Login&nbsp;&nbsp;
            <i className="material-icons">
            account_box
            </i>
          </Button>

          <Button component={Link} to="/register" size="large" variant="outlined" className={classes.button}>
          Register&nbsp;&nbsp;
            <i className="material-icons">
            create
            </i>
          </Button>
        </center>
}


function Header() {
  return (
      <center>
        <Box width="100%" bgcolor="#243743"><br/></Box>
        <Link to="/" style={{textDecoration: 'none'}}>
        <font size="+2" color="#243743">
        Trusted Members Diamond Market
        </font> <sup className="grey_it">POC - PL</sup>
        <img alt="Diamond" src={Diamond_png} width="100px" style={{position:'relative',left:'20px',top:'35px'}}></img>
        
        </Link>
      </center>
  );
}

function Footer(){
  return <center>
          Powered by
          <br/>
          <img alt="Everledger" src={el_logo}></img>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <img alt="Railsbank" src={railsbank} width="215px" style={{position:'relative',top:'15px'}}></img>
        </center>
}

export default App;